# Client

Uses ES6, Babel, and Webpack

## API

Run `npm run compile-docs` then `open ./docs/index.html`

## NPM Tasks
- Server: `npm run serve`
- Tests: `npm run test`
- Livereload Tests: `npm run test:watch`
- Compile API Docs: `npm run compile-docs`

### TODO

![Left Off](http://i.imgur.com/0soP0Fd.png)
