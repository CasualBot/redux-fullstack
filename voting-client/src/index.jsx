import React from 'react';
import Voting from './components/Voting';

const pair = ['Trainspotting', '28 Days Later'];

ReactDOM.render(
    <Voting pair={pair} winner="Trainspotting"/>,
    document.getElementById('app')
);