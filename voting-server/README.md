# Server

## API

Run `npm run compile-docs` then `open ./docs/index.html`

## NPM Tasks
- Server: `npm run serve`
- Tests: `npm run test`
- Livereload Tests: `npm run test:watch`
- Compile API Docs: `npm run compile-docs`

