import {setEntries, vote, next} from './src/core';
import {INITIAL_STATE} from './src/constants';

export default function reducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case 'SET_ENTRIES':
      return setEntries(state, action.entries);
    case 'VOTE':
      return state.update('vote', voteState => vote(voteState, action.entry));
    case 'NEXT':
      return next(state);
  }
  return state;
}