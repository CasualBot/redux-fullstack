import {Map} from 'immutable';

/**
 * File to hold the constants for the redux voting server
 */
export const INITIAL_STATE = Map();
