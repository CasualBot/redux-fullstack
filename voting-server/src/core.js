import {List, Map} from 'immutable';

/**
 * Configure the entries list
 *
 * @param state
 * @param entries
 * @returns {*}
 */
export function setEntries(state, entries) {
    return state.set('entries', List(entries));
}

/**
 * Get next entry in the state tree
 *
 * @param state
 * @returns {*}
 */
export function next(state) {
    const entries = state.get('entries')
        .concat(getWinners(state.get('vote')));

    if (entries.size === 1) {
        return state.remove('vote')
                    .remove('entries')
                    .set('winner', entries.first());
    }

    return state.merge({
        vote: Map({pair: entries.take(2)}),
        entries: entries.skip(2)
    });
}

/**
 * Add tally to entry
 *
 * @param voteState
 * @param entry
 * @returns {newState with tall increment}
 */
export function vote(voteState, entry) {

    return voteState.updateIn(
        ['tally', entry],
        0,
        tally => tally + 1
    );
}

/**
 * Count tally from votes, compare, and select winner with hightest count
 *
 * @param vote
 * @returns {*}
 */
function getWinners(vote){
    if (!vote) return []; //if no votes return empty array

    const [a, b] = vote.get('pair');
    const aVotes = vote.getIn(['tally', a], 0);
    const bVotes = vote.getIn(['tally', b], 0);

    if(aVotes > bVotes) {
        return [a];
    } else if (aVotes < bVotes) {
        return [b];
    } else {
        return [a, b];
    }

}