import {createStore} from 'redux';
import reducer from '../reducer'

/**
 *  Redux store using reducer
 * @returns {reducer}
 */
export default function makeStore() {
  return createStore(reducer);
}